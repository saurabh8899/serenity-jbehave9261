package com.pasa.steps.serenity;

import com.pasa.pages.RedditHomePage;
import com.pasa.pages.RedditLoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.*;

import javax.validation.constraints.AssertTrue;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Created by Saurabh on 1/04/2017.
 */
public class RedditSteps extends ScenarioSteps {

    RedditLoginPage redditLoginPage;
    RedditHomePage redditHomePage;

    @Step
    public void verifyUserOnRedditPage()
    {
        redditLoginPage.open();
    }



    @Step
    public void userLogsIn(String username, String password)
    {
        redditLoginPage.getUserNameField().sendKeys(username);
        redditLoginPage.getPasswordField().sendKeys(password);
        redditLoginPage.getRememberMeCheckbox().click();
        redditLoginPage.getLoginButton().click();
    }

    @Step
    public void verifyUserOnHomePage(String username)
    {
        System.out.println(username);
        assertTrue(redditHomePage.getUserNamePlaceholder().getText().equals(username));

    }

    @Step
    public void userSelectsSubredditFromDropdown(String subredditName)
    {
        redditLoginPage.getSubredditDropdown().click();
        List<WebElement> options = redditLoginPage.getSubredditDropdownList();
        for(int i=0;i<options.size();i++)
        {
            System.out.println(options.get(i).getText());
            if(options.get(i).getText().equalsIgnoreCase(subredditName))
            {
                options.get(i).click();
                break;
            }
        }

    }

    @Step
    public void verifySubredditName(String subredditName)
    {
        assertEquals(subredditName, redditLoginPage.getSubredditNamePlaceholder().getText());
    }


    @Step
    public void userProvidePreferences(ExamplesTable examplesTable)
    {
        redditHomePage.getPreferencesLink().click();
        Select select = new Select(redditHomePage.getInterfaceLanguageDropdown());
        List<WebElement> mediaOptions = redditHomePage.getMediaRadioButtons();
        List<WebElement> clickingOptions = redditHomePage.getClickingOptionsCheckBox();
        List<WebElement> linkOptions = redditHomePage.getLinkOptionsCheckbox();
        List<WebElement> privacyOptions = redditHomePage.getPrivacyOptionsCheckbox();

        for (Map<String,String> row : examplesTable.getRows()) {
            //selecting the dropdown for interface language
            select.selectByVisibleText(row.get("interface language"));

            //Selecting media options

            for(int i=0; i< mediaOptions.size(); i++)
            {
                if(mediaOptions.get(i).getText().equalsIgnoreCase(row.get("media")))
                {
                    mediaOptions.get(i).click();
                    break;
                }
            }

            // Selecting clicking options

            for(int i=0; i< clickingOptions.size(); i++)
            {
                if(clickingOptions.get(i).getText().equalsIgnoreCase(row.get("clicking options")))
                {
                    clickingOptions.get(i).click();
                    break;
                }
            }

            //Selecting link options

            for(int i=0; i< linkOptions.size(); i++)
            {
                if(linkOptions.get(i).getText().equalsIgnoreCase(row.get("link options")))
                {
                    linkOptions.get(i).click();
                    break;
                }
            }

            //Selecting privacy options

            for(int i=0; i< privacyOptions.size(); i++)
            {
                if(privacyOptions.get(i).getText().equalsIgnoreCase(row.get("privacy options")))
                {
                    privacyOptions.get(i).click();
                    break;
                }
            }
        }
    }


    @Step
    public void userSavePreferences()
    {
        redditHomePage.getSavePreferencesButton().click();
    }

}
