package com.pasa.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Saurabh on 2/04/2017.
 */
public class RedditHomePage extends PageObject {

    @FindBy(xpath="//span[@class='user']/a")
    private WebElement userNamePlaceholder;

    @FindBy(xpath="//a[@class='pref-lang choice']")
    private WebElement preferencesLink;

    @FindBy(id="lang")
    private WebElement interfaceLanguageDropdown;

    @FindBy(xpath="//th[text()='clicking options']/following-sibling::td[@class='prefright']")
    private WebElement clickingOptionsCheckBox;

    @FindBy(xpath="//th[text()='media']/following-sibling::td[@class='prefright']")
    private WebElement mediaRadioButtons;

    @FindBy(xpath="//th[text()='link options']/following-sibling::td[@class='prefright']")
    private WebElement linkOptionsCheckbox;

    @FindBy(xpath="//th[text()='privacy options']/following-sibling::td[@class='prefright']")
    private WebElement privacyOptionsCheckbox;

    @FindBy(xpath="//input[@value='save options']")
    private WebElement savePreferencesButton;







    public WebElement getPreferencesLink()
    {
        return preferencesLink;
    }

    public WebElement getUserNamePlaceholder()
    {
        return userNamePlaceholder;
    }

    public WebElement getInterfaceLanguageDropdown()
    {
        return interfaceLanguageDropdown;
    }


    public List<WebElement> getMediaRadioButtons(){
        return mediaRadioButtons.findElements(By.xpath(".//label"));
    }

    public List<WebElement> getClickingOptionsCheckBox(){
        return clickingOptionsCheckBox.findElements(By.xpath(".//label"));
    }

    public List<WebElement> getLinkOptionsCheckbox(){
        return linkOptionsCheckbox.findElements(By.xpath(".//label"));
    }

    public List<WebElement> getPrivacyOptionsCheckbox(){
        return privacyOptionsCheckbox.findElements(By.xpath(".//label"));
    }

    public WebElement getSavePreferencesButton(){
        return savePreferencesButton;
    }




}
