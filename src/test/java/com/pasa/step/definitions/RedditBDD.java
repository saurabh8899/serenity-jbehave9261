package com.pasa.step.definitions;

import com.pasa.steps.serenity.RedditSteps;
import net.serenitybdd.jbehave.SerenityJBehaveTestRunner;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;


/**
 * Created by Saurabh on 1/04/2017.
 */

@RunWith(SerenityJBehaveTestRunner.class)
public class RedditBDD {


    @Steps
    RedditSteps redditSteps;
    static String username;
    String subredditName;



    @Given("the user is on reddit website")
    public void verifyUserOnRedditPage() {
        redditSteps.verifyUserOnRedditPage();

    }

    @When("user logs in to the website with username '$username' and password '$password'")
    public void userlogsIn(String username, String password) {
        redditSteps.userLogsIn(username, password);
        this.username = username;

    }

    @Then("he should see the username on the home page")
    @Given("the user has logged onto reddit website")
    public void verifyUserNameOnHomePage() {
        redditSteps.verifyUserOnHomePage(username);

    }

    @Given("user fills the details as below preferences: $examplesTable")
    public void userProvidePreferences(ExamplesTable examplesTable) {
        redditSteps.userProvidePreferences(examplesTable);
    }


    @Given("user selects '$subredditName' subreddit from the dropdown")
    public void userSelectsSubredditFromDropdown(String subredditName) {
        redditSteps.userSelectsSubredditFromDropdown(subredditName);
        this.subredditName=subredditName;

    }

    @Then("the user should see the selected subreddit")
    public void verifySubredditName() {
        redditSteps.verifySubredditName(subredditName);


    }


    @Then("user should be able to save the preferences")
    public void userSavePreferences() {
        redditSteps.userSavePreferences();


    }





}
